import { combineReducers } from 'redux'
import { invoicesReducer, searchInInvoices } from './invoices'
import { customersReducer } from './customers'
import { productsReducer } from './products'
export const appReducer = combineReducers({
  invoicesReducer,
  searchInInvoices,
  customersReducer,
  productsReducer
})
/*
reudcer are basically simple function with a retuen value
that help our mamnagement of states
 */
