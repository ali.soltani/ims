import { ACTIONS } from '../actions/products'

const INITIAL_STATE = [
  {
    id: 1,
    name: 'smaple one',
    note: 'something for smaple one'
  },
  {
    id: 2,
    name: 'smaple two',
    note: 'something for smaple two'
  },
  {
    id: 3,
    name: 'smaple three',
    note: 'something for smaple three'
  },
  {
    id: 4,
    name: 'smaple four',
    note: 'something for smaple four'
  }
]
export const productsReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ACTIONS.SHOW_ALL_PRODUCTS:
      return state
    default:
      return state
  }
}
