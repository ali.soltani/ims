import {ACTIONS} from '../actions/customers'

const INITIAL_STATE = [
  {
    id: 1,
    name: 'john',
    email: 'john@test.com'
  },
  {
    id: 2,
    name: 'chris',
    email: 'chris@test.com'
  },
  {
    id: 3,
    name: 'temp',
    email: 'xoratuxac@taylorventuresllc.com'
  }
]

export const customersReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ACTIONS.SHOW_ALL_CUSTOMERS:
      return state
    default:
      return state
  }
}
