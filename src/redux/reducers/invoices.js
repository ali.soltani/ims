import { ACTIONS } from '../actions/invoices'

const INITIAL_STATE = []

export const invoicesReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ACTIONS.ADD_INVOICE:
      return [...state, action.invoice]
    case ACTIONS.REMOVE_INVOICE:
      return state.filter(invoice => invoice.id !== action.invoiceId)
    case ACTIONS.FINALIZE_INVOICE:
      return state.map(
        invoice =>
          invoice.id === action.invoiceId
            ? Object.assign({}, invoice, { isFinalize: true })
            : invoice
      )
    case ACTIONS.SHOW_ALL_INVOICES:
      return action.invoices
    default:
      return state
  }
}

export const searchInInvoices = (state = {}, action) => {
  switch (action.type) {
    case ACTIONS.FIND_ONE_INVOICE:
      return { invoiceId: action.invoiceId }
    default:
      return state
  }
}
