export const ACTIONS = {
  SHOW_ALL_CUSTOMERS: 'SHOW_ALL_CUSTOMERS'
}

export const showAllCustomers = () => {
  return {
    type: ACTIONS.SHOW_ALL_CUSTOMERS
  }
}
