export const ACTIONS = {
  SHOW_ALL_PRODUCTS: 'SHOW_ALL_PRODUCTS'
}

export const showAllProducts = () => {
  return {
    type: ACTIONS.SHOW_ALL_PRODUCTS
  }
}
