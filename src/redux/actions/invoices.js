import {
  addInvoiceAsync,
  removeInvoiceByIdAsync,
  showAllInvoices as showAll,
  finalizeInvoiceAsync
} from '../../App/firebase/invoice'
export const ACTIONS = {
  ADD_INVOICE: 'ADD_INVOICE',
  REMOVE_INVOICE: 'REMOVE_INVOICE',
  FIND_ONE_INVOICE: 'FIND_ONE_INVOICE',
  FINALIZE_INVOICE: 'FINALIZE_INVOICE',
  SHOW_ALL_INVOICES: 'SHOW_ALL_INVOICES'
}
/*
  all functions that start with _ are actions that call reducer
  basically functions that return {type:....} works with reducer

  functions that have dispatch are ones that work with database
  they are async.some with callback.they with dispatch another
  function to access reducer
*/
const _finalizeInvoice = invoiceId => {
  return {
    type: ACTIONS.FINALIZE_INVOICE,
    invoiceId
  }
}
export const finalizeInvoice = invoice => {
  return async dispatch => {
    await finalizeInvoiceAsync(invoice)
    dispatch(_finalizeInvoice(invoice.id))
  }
}
const _addInvoice = invoice => {
  return {
    type: ACTIONS.ADD_INVOICE,
    invoice
  }
}
export const addInvoice = invoice => {
  return async dispatch => {
    const result = await addInvoiceAsync(invoice)
    const id = result.key
    dispatch(_addInvoice(Object.assign({}, invoice, { id })))
    return id
  }
}

const _removeInvoice = invoiceId => {
  return {
    type: ACTIONS.REMOVE_INVOICE,
    invoiceId
  }
}
export const removeInvoice = invoiceId => {
  return async dispatch => {
    await removeInvoiceByIdAsync(invoiceId)
    dispatch(_removeInvoice(invoiceId))
  }
}
export const findOneInvoice = invoiceId => {
  return {
    type: ACTIONS.FIND_ONE_INVOICE,
    invoiceId
  }
}

const mapInvoiceObjectToArray = invoicesObjec => {
  const keyArray = Object.keys(invoicesObjec)
  const invoices = keyArray.map(key =>
    Object.assign({}, invoicesObjec[key], { id: key })
  )
  return invoices
}
const _showAllInvoices = invoices => {
  return {
    type: ACTIONS.SHOW_ALL_INVOICES,
    invoices
  }
}
export const showAllInvoices = () => {
  return async dispatch => {
    showAll(data => {
      const invoicesObject = data.val()
      dispatch(_showAllInvoices(mapInvoiceObjectToArray(invoicesObject)))
    })
  }
}
