import * as firebase from 'firebase'
const config = {
  apiKey: 'AIzaSyCmaOO-DlSez0Qf03KUsLLDURxCQgv94ME',
  authDomain: 'invoice-b3c6c.firebaseapp.com',
  databaseURL: 'https://invoice-b3c6c.firebaseio.com',
  projectId: 'invoice-b3c6c',
  storageBucket: 'invoice-b3c6c.appspot.com',
  messagingSenderId: '776941165461'
}

firebase.initializeApp(config)

export default firebase
