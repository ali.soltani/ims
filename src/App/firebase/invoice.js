import firebase from './driver'
const invoiceRef = firebase.database().ref('/invoices')
// use created firebase and set it to invoices

export const addInvoiceAsync = invoice => {
  return invoiceRef.push(invoice)
}
export const showAllInvoices = callback => {
  return invoiceRef.once('value', callback)
}
export const findInvoiceById = (invoiceId, callback) => {
  // in invoices select invoiceId
  return invoiceRef.child(invoiceId).once('value', callback)
}
export const removeInvoiceByIdAsync = invoiceId => {
  return invoiceRef.child(invoiceId).remove()
}
export const showCustomerInvoices = (customerId, callback) => {
  return invoiceRef
    .orderByChild('customer/id')
    .equalTo(customerId)
    .once('value', callback)
}
export const finalizeInvoiceAsync = async invoice => {
  if (invoice.isFinalize) {
    return invoice
  }
  const finalizedInvoice = Object.assign({}, invoice, {isFinalize: true})
  return invoiceRef.child(invoice.id).set(finalizedInvoice)
}
