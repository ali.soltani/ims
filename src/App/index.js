import React from 'react'
import { createStackNavigator } from 'react-navigation'
// importing our app pages
import Home from './home'
import Customers from './customers'
import PrepareInvoice from './invoices/Prepare'
import FinilizeInvoice from './invoices/Finilize'
import PreviewInvoice from './invoices/Preview'
import DraftList from './invoices/DraftList'
import InvoiceList from './invoices/InvoiceList'

// list of routes. each page needs to be specified here and in RootStack in order to be reachable
// it can be imported any where to help routing
export const ROUTE_LIST = {
  HOME: 'Home',
  CUSTOMERS: 'Customers',
  PREPARE_INVOICE: 'PrepareInvoice',
  FINILIZE_INVOICE: 'FinilizeInvoice',
  PREVIEW_INVOICE: 'PreviewInvoice',
  DRAFT_LIST: 'DraftList',
  INVOICE_LIST: 'InvoiceList'
}
const RootStack = createStackNavigator(
  {
    [ROUTE_LIST.HOME]: Home,
    [ROUTE_LIST.CUSTOMERS]: Customers,
    [ROUTE_LIST.PREPARE_INVOICE]: PrepareInvoice,
    [ROUTE_LIST.FINILIZE_INVOICE]: FinilizeInvoice,
    [ROUTE_LIST.PREVIEW_INVOICE]: PreviewInvoice,
    [ROUTE_LIST.DRAFT_LIST]: DraftList,
    [ROUTE_LIST.INVOICE_LIST]: InvoiceList
  },
  {
    initialRouteName: ROUTE_LIST.HOME // define main page
  }
)
// from 'Home' > select customer from 'Customers' > select products in 'PrepareInvoice'
// ... > modify products in 'FinilizeInvoice'.it will be saved as Draft too
// ... > last step in 'PreviewInvoice' you can send it as pdf,
// finilize it or just keep it as draf or remove it
class App extends React.Component {
  render () { // handling routing with help of 'react-navigation'
    return <RootStack />
    // navigation.navigate is accessible from prop in components
    // since thy are rapped in react-navigation
    // for navigatign to any page should use navigation.navigate(<name of route>)
    // which name of routes are available in ROUTE_LIST
  }
}

export default App
