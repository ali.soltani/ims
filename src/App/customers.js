import React from 'react'
import { StyleSheet, Text, ListView, TouchableHighlight } from 'react-native'
import { connect } from 'react-redux'
import { ROUTE_LIST } from './index'

class Customers extends React.Component {
  _pressRow (customer) {
    this.props.navigation.navigate(ROUTE_LIST.PREPARE_INVOICE, customer)
    // sends customer to prepare page in invoices/Prepare.js
  }
  render () {
    const { customers } = this.props
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
    const customersDataSource = ds.cloneWithRows(customers)
    return (
      <ListView
        dataSource={customersDataSource}
        renderRow={customer => (
          <TouchableHighlight onPress={() => this._pressRow(customer)}>
            <Text style={styles.container}>{customer.name}</Text>
          </TouchableHighlight>
        )}
      />
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    fontWeight: 'bold',
    padding: 5,
    fontSize: 25
  }
})
const mapStateToProps = state => {
  const customers = state.customersReducer
  // gets current list of customers from reducer named customersReducer
  return {
    customers // it is accessible from props with name of customers
  }
}
export default connect(mapStateToProps, null)(Customers)
// connects our component with redux.
