import React from 'react'
import { StyleSheet, Text, View, ListView } from 'react-native'
import { showAllInvoices } from '../../redux/actions/invoices'
import { connect } from 'react-redux'

class InvoiceList extends React.Component {
  componentDidMount () {
    const { invoices, showAllInvoices } = this.props
    if (invoices.length === 0) {
      showAllInvoices()
    }
  }
  render () {
    const { invoices } = this.props
    const finilizedInvoices = invoices.filter(invc => invc.isFinalize)
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
    const dataSource = ds.cloneWithRows(finilizedInvoices)
    return (
      <View style={styles.container}>
        <Text>DraftList</Text>
        {finilizedInvoices.length > 0 ? (
          <ListView
            dataSource={dataSource}
            renderRow={rowData => (
              <View
                style={{
                  borderWidth: 1,
                  borderColor: '#ff00ff'
                }}
              >
                <Text key='id' style={styles.container}>
                  {rowData.id}
                </Text>
                <Text key='name' style={styles.container}>
                  {rowData.customer.name}
                </Text>
                {rowData.products.map((product, index) => (
                  <View key={`${rowData.id}-products-${index}`}>
                    <Text key={`name-${product.id}`} style={styles.container}>
                      {product.name}
                    </Text>
                    <Text
                      key={`unitPrice-${product.id}`}
                      style={styles.container}
                    >
                      {product.unitPrice}
                    </Text>
                  </View>
                ))}
              </View>
            )}
          />
        ) : invoices.length > 0 ? (
          <Text style={styles.container}>no data</Text>
        ) : (
          <Text style={styles.container}>loading ...</Text>
        )}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
})

const mapStateToProps = state => {
  const invoices = state.invoicesReducer
  return {
    invoices
  }
}
export default connect(
  mapStateToProps,
  { showAllInvoices }
)(InvoiceList)
