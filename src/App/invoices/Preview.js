import React from 'react'
import { StyleSheet, Text, View, Button, ListView, Alert } from 'react-native'
import { ROUTE_LIST } from '../index'
import {
  findOneInvoice,
  finalizeInvoice,
  removeInvoice
} from '../../redux/actions/invoices'
import { connect } from 'react-redux'
import RNHTMLtoPDF from 'react-native-html-to-pdf'
import Mailer from 'react-native-mail'
import customers from '../customers'

class Preview extends React.Component {
  constructor (props) {
    super(props)

    this._finalizeInvoice = this._finalizeInvoice.bind(this)
    this._finalizeInvoiceAndSavePDF = this._finalizeInvoiceAndSavePDF.bind(this)
    this._finalizeInvoiceAndSavePDFAndSendViaEmail = this._finalizeInvoiceAndSavePDFAndSendViaEmail.bind(
      this
    )
    this._saveAsPDF = this._saveAsPDF.bind(this)
    this._sendViaEmail = this._sendViaEmail.bind(this)
    this._removeDraft = this._removeDraft.bind(this)
  }
  componentDidMount () {
    const { navigation } = this.props
    const id = navigation.getParam('id', {})
    this.props.findOneInvoice(id)
    // find invoice with id given from previous page
  }

  async _saveAsPDF () {
    let options = {
      html: '<h1>PDF TEST</h1>',
      fileName: `invoice ${this.props.invoice.id}`,
      directory: 'docs'
    }
    let file = await RNHTMLtoPDF.convert(options)
    return file.filePath
  }
  _sendViaEmail (filePath) {
    const { invoice } = this.props
    const { customer } = invoice

    Mailer.mail(
      {
        subject: 'your incvoice',
        recipients: [customer.email],
        body: '<b>here is pdf file of your invoice</b>',
        isHTML: true,
        attachment: {
          path: filePath,
          type: 'pdf'
        }
      },
      (error, event) => {
        Alert.alert(
          error,
          event,
          [
            {
              text: 'Ok',
              onPress: () => console.log('OK: Email Error Response')
            },
            {
              text: 'Cancel',
              onPress: () => console.log('CANCEL: Email Error Response')
            }
          ],
          { cancelable: true }
        )
      }
    )
  }
  _returnHome () {
    this.props.navigation.navigate(ROUTE_LIST.HOME)
  }
  _finalizeInvoice () {
    const {invoice} = this.props
    this.props.finalizeInvoice(invoice)
  }
  async _finalizeInvoiceAndSavePDF () {
    await this._finalizeInvoice()
    // await this._saveAsPDF()
    this._returnHome()
  }
  async _finalizeInvoiceAndSavePDFAndSendViaEmail () {
    await this._finalizeInvoice()
    // const pdfPath = await this._saveAsPDF()
    // this._sendViaEmail(pdfPath)
    this._returnHome()
  }
  _removeDraft () {
    const { navigation, removeInvoice } = this.props
    const id = navigation.getParam('id', {})
    removeInvoice(id)
    this._returnHome()
  }
  _calculateTotalPrice (products) {
    let totallPrice = 0
    products.forEach(product => {
      const { unitPrice, isTaxIncluded, unitQuantity } = product
      let price = Number(unitPrice) * Number(unitQuantity)
      if (isTaxIncluded) {
        const tax = (price * 13) / 100
        price += tax
      }
      totallPrice += price
    })
    return totallPrice || 0
  }

  render () {
    const customer = this.props.invoice
      ? this.props.invoice.customer
      : undefined
    const products = this.props.invoice
      ? Array.from(this.props.invoice.products)
      : []
    let totallPrice = 0
    if (customers) {
      totallPrice = this._calculateTotalPrice(products)
    }
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
    const productsDataSource = ds.cloneWithRows(products)
    return (
      <View style={styles.container}>
        {customer ? (
          <React.Fragment>
            <Text>{customer.name}</Text>
            <Text>{customer.email}</Text>
            <ListView
              dataSource={productsDataSource}
              renderRow={rowData => (
                <React.Fragment>
                  <Text style={styles.container}>{rowData.name}</Text>
                  <Text style={styles.container}>
                    {rowData.isTaxIncluded ? 'Tax' : 'No Tax'}
                  </Text>
                  <Text style={styles.container}>{rowData.unitPrice}</Text>
                  <Text style={styles.container}>{rowData.unitQuantity}</Text>
                </React.Fragment>
              )}
            />
            <Text>{`totall price is ${totallPrice}`}</Text>
          </React.Fragment>
        ) : (
          undefined
        )}
        <Button
          title='Save To Drafts'
          onPress={() => this.props.navigation.navigate(ROUTE_LIST.HOME)}
        />
        <Button
          title='Save As PDF'
          onPress={this._finalizeInvoiceAndSavePDF}
        />
        <Button
          title='Send PDF via Email'
          onPress={this._finalizeInvoiceAndSavePDFAndSendViaEmail}
        />
        <Button
          title='Remve'
          onPress={this._removeDraft}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
})

const mapStateToProps = state => {
  const id = state.searchInInvoices.invoiceId
  const invoice = state.invoicesReducer.find(invoice => invoice.id === id)
  return {
    invoice
  }
}

export default connect(
  mapStateToProps,
  { findOneInvoice, finalizeInvoice, removeInvoice }
)(Preview)
