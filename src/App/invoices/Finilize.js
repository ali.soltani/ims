import React from 'react'
import {
  StyleSheet,
  Text,
  View,
  Button,
  ListView,
  TextInput
} from 'react-native'
import { ROUTE_LIST } from '../index'
import { find, findIndex } from 'lodash'
import ToggleSwitch from 'toggle-switch-react-native'
import { connect } from 'react-redux'
import { addInvoice } from '../../redux/actions/invoices'

class Finilize extends React.Component {
  constructor (props) {
    super(props)
    const { navigation } = props
    const invoice = navigation.getParam('invoice', {})
    const { products } = invoice
    const { customer } = invoice
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
    const dataSource = ds.cloneWithRows(products)
    this.state = {
      products: products.map(prdct =>
        Object.assign({}, prdct, {
          isTaxIncluded: false,
          unitPrice: '10',
          unitQuantity: '1'
        }) // set default value for price and quantity and tax
      ),
      dataSource,
      customer
    }
    this._editProductListWithNewFiled = this._editProductListWithNewFiled.bind(
      this
    )
    this._handleNameChange = this._handleNameChange.bind(this)
    this._handleTaxInput = this._handleTaxInput.bind(this)
    this._handlePriceChange = this._handlePriceChange.bind(this)
    this._handleQuantityChange = this._handleQuantityChange.bind(this)
    this._sendToPreviewPage = this._sendToPreviewPage.bind(this)
  }
  _editProductListWithNewFiled (productId, field) {
    // gets productId and object of field and its value and edit the list
    const products = Array.from(this.state.products)
    // create new list of products
    const index = findIndex(products, prdct => prdct.id === productId)
    // find index of old product
    const newProduct = Object.assign({}, products[index], field)
    // create new product
    products.splice(index, 1, newProduct)
    // replace it in list
    this.setState({
      products // save it in state
    })
  }
  _handleNameChange (name, productId) {
    this._editProductListWithNewFiled(productId, { name })
  }
  _handleTaxInput (isTaxIncluded, productId) {
    this._editProductListWithNewFiled(productId, { isTaxIncluded })
  }
  _handlePriceChange (unitPrice, productId) {
    this._editProductListWithNewFiled(productId, { unitPrice })
  }
  _handleQuantityChange (unitQuantity, productId) {
    this._editProductListWithNewFiled(productId, { unitQuantity })
  }
  async _sendToPreviewPage () {
    const { customer, products } = this.state
    const invoice = {
      customer,
      products,
      isFinalize: false
    }
    const id = await this.props.addInvoice(invoice)
    // save it in drafts and send the id to Preview.js
    this.props.navigation.navigate(ROUTE_LIST.PREVIEW_INVOICE, {
      id
    })
  }
  render () {
    return (
      <View>
        <Text>{`${this.state.customer.name}`}</Text>
        <ListView
          dataSource={this.state.dataSource}
          renderRow={product => (
            <View style={styles.container}>
              <View>
                <Text>product name :</Text>
                <TextInput
                  value={
                    find(this.state.products, prdct => prdct.id === product.id)
                      .name // find product name in state
                  }
                  onChangeText={text =>
                    this._handleNameChange(text, product.id)
                  } // call this._handleNameChange to edit product name
                />
              </View>
              <View>
                <ToggleSwitch
                  isOn={
                    find(this.state.products, prdct => prdct.id === product.id)
                      .isTaxIncluded
                  }
                  label='Tax Included :'
                  onToggle={value => this._handleTaxInput(value, product.id)}
                />
              </View>
              <View>
                <Text>unit price :</Text>
                <TextInput
                  value={
                    find(this.state.products, prdct => prdct.id === product.id)
                      .unitPrice
                  }
                  keyboardType='number-pad'
                  onChangeText={text =>
                    this._handlePriceChange(text, product.id)
                  }
                />
              </View>
              <View>
                <Text>unit quantity :</Text>
                <TextInput
                  value={
                    find(this.state.products, prdct => prdct.id === product.id)
                      .unitQuantity
                  }
                  keyboardType='number-pad'
                  onChangeText={text =>
                    this._handleQuantityChange(text, product.id)
                  }
                />
              </View>
            </View>
          )}
        />
        <Button title='Preview Invoice' onPress={this._sendToPreviewPage} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 2,
    borderColor: '#00ff00'
  }
})

export default connect(
  null,
  { addInvoice } // connect addInvoice to component. it is accessible from props
)(Finilize)
