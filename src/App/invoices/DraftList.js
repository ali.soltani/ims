import React from 'react'
import {
  StyleSheet,
  Text,
  View,
  ListView,
  TouchableHighlight
} from 'react-native'
import { showAllInvoices } from '../../redux/actions/invoices'
import { connect } from 'react-redux'
import { ROUTE_LIST } from '../index'

class DraftList extends React.Component {
  componentDidMount () {
    const { invoices, showAllInvoices } = this.props
    if (invoices.length === 0) {
      showAllInvoices()
    }
  }
  _pressRow (draft) {
    const { id } = draft
    this.props.navigation.navigate(ROUTE_LIST.PREVIEW_INVOICE, {
      id
    })
  }
  render () {
    const { invoices } = this.props
    const drafts = invoices.filter(invc => !invc.isFinalize)
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
    const dataSource = ds.cloneWithRows(drafts)
    return (
      <View style={styles.container}>
        <Text>DraftList</Text>
        {drafts.length > 0 ? (
          <ListView
            dataSource={dataSource}
            renderRow={rowData => (
              <TouchableHighlight onPress={() => this._pressRow(rowData)}>
                <View
                  style={{
                    borderWidth: 1,
                    borderColor: '#ff00ff'
                  }}
                >
                  <Text key='id' style={styles.container}>
                    {rowData.id}
                  </Text>
                  <Text key='name' style={styles.container}>
                    {rowData.customer.name}
                  </Text>
                  {rowData.products.map((product, index) => (
                    <View key={`${rowData.id}-products-${index}`}>
                      <Text key={`name-${product.id}`} style={styles.container}>
                        {product.name}
                      </Text>
                      <Text
                        key={`unitPrice-${product.id}`}
                        style={styles.container}
                      >
                        {product.unitPrice}
                      </Text>
                    </View>
                  ))}
                </View>
              </TouchableHighlight>
            )}
          />
        ) : invoices.length > 0 ? (
          <Text style={styles.container}>no data</Text>
        ) : (
          <Text style={styles.container}>loading ...</Text>
        )}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
})

const mapStateToProps = state => {
  const invoices = state.invoicesReducer
  return {
    invoices
  }
}
export default connect(
  mapStateToProps,
  { showAllInvoices }
)(DraftList)
