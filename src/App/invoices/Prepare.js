import React from 'react'
import {
  StyleSheet,
  Text,
  View,
  ListView,
  TouchableHighlight,
  Button
} from 'react-native'
import { connect } from 'react-redux'
import { ROUTE_LIST } from '../index'

class Add extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      selectedProducts: [],
      styles: [],
      hasError: false
    }
    this._pressRow = this._pressRow.bind(this)
    this._productStyleInRow = this._productStyleInRow.bind(this)
    this._prepareAndSendInvoice = this._prepareAndSendInvoice.bind(this)
  }
  _generateNewSelectedProductList (product) {
    const productHasFound = this.state.selectedProducts.find(
      prdct => prdct.id === product.id
    )
    if (productHasFound) {
      return this.state.selectedProducts.filter(
        prdct => prdct.id !== product.id
      )
    }
    return [...this.state.selectedProducts, product]
  }
  _pressRow (product) {
    const selectedProducts = this._generateNewSelectedProductList(product)
    // check if product is in selected list remove it otherwise add it to list
    this.setState({
      selectedProducts
    })
    this._productStyleInRow(product) // create style for each product in list
  }
  componentDidMount () {
    const { products } = this.props
    let styles = {}
    products.forEach(prdct => {
      styles[prdct.id] = {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        fontWeight: 'bold',
        padding: 5,
        fontSize: 25
      }
    })
    // set default style for each product row
  }
  _productStyleInRow (product) {
    const productHasFound = this.state.selectedProducts.find(
      prdct => prdct.id === product.id
    )
    const style = Object.assign({}, this.state[product.id], {
      backgroundColor: productHasFound ? 'transparent' : '#ff0000'
    })
    // change style of product in row based on if it is selected
    this.setState({
      styles: Object.assign({}, this.state.styles, { [product.id]: style })
    })
  }
  _prepareAndSendInvoice () {
    const { selectedProducts } = this.state
    if (selectedProducts.length === 0) {
      // make sure product list is not empty
      this.setState({
        hasError: true
      })
      return
    }
    const { navigation } = this.props
    const customerId = navigation.getParam('id', undefined)
    const name = navigation.getParam('name', undefined)
    const email = navigation.getParam('email', undefined)
    const invoice = {
      customer: {
        id: customerId,
        name,
        email
      },
      products: selectedProducts
    } // generating new invoice
    this.props.navigation.navigate(ROUTE_LIST.FINILIZE_INVOICE, { invoice })
    // pass it to Finilize.js
  }
  render () {
    const { navigation, products } = this.props
    const name = navigation.getParam('name', undefined) // get what ever passed whith navigation
    const email = navigation.getParam('email', undefined)
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
    const productsDataSource = ds.cloneWithRows(products)
    return (
      <React.Fragment>
        <View>
          <Text style={styles.container}>{`${name} ${email}`}</Text>
          <Text style={styles.container}>{`${
            this.state.selectedProducts.length
          } item(s) selected`}</Text>
          <Button title='Procced' onPress={this._prepareAndSendInvoice} />
          {this.state.hasError ? ( // check selected product list is not empty
            <Text style={styles.error}>selected items can not be empty</Text>
          ) : (
            undefined
          )}
        </View>
        <ListView
          dataSource={productsDataSource}
          renderRow={product => (
            <TouchableHighlight onPress={() => this._pressRow(product)}>
              <Text style={this.state.styles[product.id]}>{product.name}</Text>
            </TouchableHighlight>
          )}
        />
      </React.Fragment>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 25
  },
  error: {
    fontSize: 20,
    color: '#f00'
  }
})
const mapStateToProps = state => {
  const products = state.productsReducer
  // gets current list of products from reducer named customersReducer

  return {
    products // it is accessible from props with name of products
  }
}
export default connect(
  mapStateToProps,
  null
)(Add)
// connects our component with redux.
