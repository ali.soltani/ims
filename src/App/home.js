import React from 'react'
import { StyleSheet, View, Button } from 'react-native'
import {ROUTE_LIST} from './index' // list of routes

class Home extends React.Component {
  render () {
    return (
      <View style={styles.container}>
        <Button
          title='Customer List'
          onPress={() => this.props.navigation.navigate(ROUTE_LIST.CUSTOMERS)}
        />
        <Button
          title='Draft List'
          onPress={() => this.props.navigation.navigate(ROUTE_LIST.DRAFT_LIST)}
        />
        <Button
          title='Invoice List'
          onPress={() => this.props.navigation.navigate(ROUTE_LIST.INVOICE_LIST)}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
})

export default Home
