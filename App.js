import React from 'react'
import App from './src/App'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import { appReducer } from './src/redux/reducers' // reducers are combined
import ReduxThunk from 'redux-thunk'

const store = createStore(appReducer, applyMiddleware(ReduxThunk)) // store will be created

export default () => (
  <Provider store={store}>
    <App />
  </Provider>
)// whole app is rapped with redux check out src/redux
